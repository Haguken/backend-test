from django.db.models import base
import grequests
import json
from datetime import datetime
import base64
import requests


def get_binary_responses(urls: list) -> list:
    reqs = (grequests.get(url) for url in urls)
    return grequests.map(reqs)

def get_dicts_from_binary_responses(binary_responses: list) -> list:
    return [json.loads(br._content.decode('utf-8')) for br in binary_responses]


def get_nested_dicts_from_model(urls: list) -> list:
    bin_res = get_binary_responses(urls)
    nested_list = get_dicts_from_binary_responses(bin_res)
    return nested_list


def get_book(id: int):
    books = get_nested_dicts_from_model([f'https://anapioficeandfire.com/api/books/{id}'])
    book = books[0]
    book['id'] = int(book.pop('url').split('/')[-1])
    book['cover'] = base64.b64encode(requests.get(f"http://covers.openlibrary.org/b/isbn/{book['isbn']}-L.jpg").content)
    return book


def get_characters_from_book(book_id: int):
    book = get_book(book_id)
    characters = [get_nested_dicts_from_model(book['characters'])][0]
    for character in characters:
        character['id'] = int(character.pop('url').split('/')[-1])
    return characters


def create_povcharacters_links_for_book(book: dict, dominium='local') -> None:
    povchars_links = book['povCharacters']
    links = []
    for povchar_link in povchars_links:
        if dominium == 'local':
            links.append(f"http://127.0.0.1:8000/api/characters/{povchar_link.split('/')[-1]}")
        else:
            links.append(f"https://{dominium}/api/characters/{povchar_link.split('/')[-1]}")
    book['povCharacters'] = links


def create_characters_links_for_book(book: dict, dominium='local') -> None:
    chars_links = book['characters']
    links = []
    for char_link in chars_links:
        if dominium == 'local':
            links.append(f"http://127.0.0.1:8000/api/characters/{char_link.split('/')[-1]}")
        else:
            links.append(f"https://{dominium}/api/characters/{char_link.split('/')[-1]}")
    book['characters'] = links


def create_links_for_book(book: dict, dominium='local') -> None:
    create_povcharacters_links_for_book(book, dominium)
    create_characters_links_for_book(book, dominium)
    


def create_povbooks_links_for_char(char: dict, dominium='local') -> None:
    povbooks = char['povBooks']
    links = []
    for povbook_link in povbooks:
        if dominium == 'local':
            links.append(f"http://127.0.0.1:8000/api/books/{povbook_link.split('/')[-1]}")
        else:
            links.append(f"https://{dominium}/api/books/{povbook_link.split('/')[-1]}")
    char['povBooks'] = links


def create_books_links_for_char(char: dict, dominium='local') -> None:
    books = char['books']
    links = []
    for book_link in books:
        if dominium == 'local':
            links.append(f"http://127.0.0.1:8000/api/books/{book_link.split('/')[-1]}")
        else:
            links.append(f"https://{dominium}/api/books/{book_link.split('/')[-1]}")
    char['books'] = links


def create_links_for_char(char: dict, dominium='local') -> None:
    create_books_links_for_char(char, dominium)
    create_povbooks_links_for_char(char, dominium)
    infos = [char['father'], char['mother'], char['spouse']]
    new_infos = []
    for info in infos:
        if info:
            if dominium == 'local':
                info = f"http://127.0.0.1:8000/api/characters/{info.split('/')[-1]}"
            else:
                info = f"https://{dominium}/api/characters/{info.split('/')[-1]}"
        new_infos.append(info)
    [char['father'], char['mother'], char['spouse']] = new_infos
    char_allegiances = char['allegiances']
    new_allegiances = []
    for allegiance in char_allegiances:
        if allegiance:
            if dominium == 'local':
                allegiance = f"http://127.0.0.1:8000/api/houses/{allegiance.split('/')[-1]}"
            else:
                allegiance = f"https://{dominium}/api/houses/{allegiance.split('/')[-1]}"
        new_allegiances.append(allegiance)
    char['allegiances'] = new_allegiances



def get_povcharacters_from_book(book_id: int):
    book = get_book(book_id)
    characters = [get_nested_dicts_from_model(book['povCharacters'])][0]
    for character in characters:
        character['id'] = int(character.pop('url').split('/')[-1])
    return characters





