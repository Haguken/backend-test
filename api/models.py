from django.db import models


class Character(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    gender = models.CharField(max_length=255)
    culture = models.CharField(max_length=255)
    born = models.CharField(max_length=255)
    died = models.CharField(max_length=255)
    titles = models.JSONField()
    aliases = models.JSONField()
    father = models.CharField(max_length=255)
    mother = models.CharField(max_length=255)
    spouse = models.CharField(max_length=255)
    allegiances = models.JSONField(null=True)
    books = models.JSONField(null=True)
    povBooks = models.JSONField(null=True)
    tvSeries = models.JSONField()
    playedBy = models.JSONField()


# class House(models.Model):
#     id = models.IntegerField(primary_key=True)
#     name = models.CharField(max_length=255)
#     region = models.CharField(max_length=255)
#     coatOfArms = models.CharField(max_length=255)
#     words = models.CharField(max_length=255)
#     titles = models.JSONField()
#     seats = models.JSONField()
#     currentLord = models.OneToOneField(Character, on_delete=models.CASCADE, related_name='currentLordOf', null=True, blank=True)
#     heir = models.OneToOneField(Character, on_delete=models.CASCADE, null=True, blank=True)
#     overlord = models.CharField(max_length=255)
#     founded = models.CharField(max_length=255)
#     founder = models.OneToOneField(Character, on_delete=models.CASCADE, related_name='foundedHouse', null=True, blank=True)
#     diedOut = models.CharField(max_length=255)
#     ancestralWeapons = models.JSONField()
#     cadetBranches = models.ForeignKey('self', on_delete=models.CASCADE, related_name='monarchBranch', null=True, blank=True)
#     swornMembers = models.ForeignKey(Character, on_delete=models.CASCADE, related_name='swornHouse', null=True, blank=True)
#     allegiance_characters = models.ManyToManyField(Character, related_name='allegiances', blank=True)
    

class Book(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    isbn = models.CharField(max_length=255)
    authors = models.JSONField()
    numberOfPages = models.IntegerField()
    publisher = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    mediaType = models.CharField(max_length=255)
    released = models.JSONField()
    characters = models.JSONField(null=True)
    povCharacters = models.JSONField(null=True)
    cover = models.TextField(null=True)



