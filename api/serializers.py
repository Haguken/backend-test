from api.models import Character, House, Book
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField


class HouseSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=255)
    region = serializers.CharField(max_length=255)
    coatOfArms = serializers.CharField(max_length=255)
    words = serializers.CharField(max_length=255)
    titles = serializers.JSONField()
    seats = serializers.JSONField()
    currentLord = serializers.CharField(max_length=255)
    heir = serializers.CharField(max_length=255)
    overlord = RecursiveField()
    founded = serializers.CharField(max_length=255)
    diedOut = serializers.CharField(max_length=255)
    ancestralWeapons = serializers.JSONField()
    cadetBranches = serializers.JSONField()
    def create(self, validated_data):
        pass


class CharacterSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    url = serializers.CharField(max_length=255)
    name = serializers.CharField(max_length=255)
    gender = serializers.CharField(max_length=255)
    culture = serializers.CharField(max_length=255)
    born = serializers.CharField(max_length=255)
    died = serializers.CharField(max_length=255)
    titles = serializers.JSONField()
    aliases = serializers.JSONField()
    father = serializers.CharField(max_length=255)
    mother = serializers.CharField(max_length=255)
    spouse = serializers.CharField(max_length=255)
    allegiances = HouseSerializer()
    tvSeries = serializers.JSONField()
    playedBy = serializers.JSONField()
    founderedHouses = HouseSerializer()
    swornHouses = HouseSerializer()
    def create(self, validated_data):
        pass


class BookSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=255)
    isbn = serializers.CharField(max_length=255)
    authors = serializers.JSONField()
    numberOfPages = serializers.IntegerField()
    publisher = serializers.CharField(max_length=255)
    country = serializers.CharField(max_length=255)
    mediaType = serializers.CharField(max_length=255)
    released = serializers.DateTimeField()
    characters = CharacterSerializer()
    povCharacters = CharacterSerializer()
    def create(self, validated_data):
        pass
